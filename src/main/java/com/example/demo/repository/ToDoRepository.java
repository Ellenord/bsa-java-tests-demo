package com.example.demo.repository;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {

    List < ToDoEntity > findByText(String text);

    void deleteByText(String text);

}
