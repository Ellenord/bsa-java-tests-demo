package com.example.demo.exception;

public class ToDoEmptyTextException extends Exception {

    private static final long serialVersionUID = -186139195386774362L;

    public ToDoEmptyTextException() {
        super("Empty text is not supported.");
    }

}
