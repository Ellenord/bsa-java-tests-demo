package com.example.demo.dto;

import javax.validation.constraints.NotNull;

public class TextDto {
    @NotNull
    public String text;
}
