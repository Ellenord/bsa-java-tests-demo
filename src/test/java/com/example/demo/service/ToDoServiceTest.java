package com.example.demo.service;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoEmptyTextException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

//import static org.mockito.AdditionalAnswers.*;

class ToDoServiceTest {

	private ToDoRepository toDoRepository;

	private ToDoService toDoService;

	//executes before each test defined below
	@BeforeEach
	void setUp() {
		this.toDoRepository = mock(ToDoRepository.class);
		toDoService = new ToDoService(toDoRepository);
	}

	@Test
	void whenGetByText_thenReturnListOfToDos() throws ToDoEmptyTextException {

		List < ToDoEntity > todos = new ArrayList<>();

		todos.add(new ToDoEntity(1l, "kek"));
		todos.add(new ToDoEntity(3l, "kek"));

		when(toDoRepository.findByText(anyString())).thenReturn(todos);

		var result = toDoService.getByText("lol");

		assertTrue(result != null && todos != null);
		assertTrue(result.size() == todos.size());
		assertTrue(result.size() == 2);

		assertTrue(ToDoEntityToResponseMapper.map(todos.get(0)).id == result.get(0).id);
		assertTrue(ToDoEntityToResponseMapper.map(todos.get(1)).id == result.get(1).id);

		assertTrue(ToDoEntityToResponseMapper.map(todos.get(0)).text == result.get(0).text);
		assertTrue(ToDoEntityToResponseMapper.map(todos.get(1)).text == result.get(1).text);

		assertTrue(ToDoEntityToResponseMapper.map(todos.get(0)).completedAt == result.get(0).completedAt);
		assertTrue(ToDoEntityToResponseMapper.map(todos.get(1)).completedAt == result.get(1).completedAt);

	}

	@Test
	void whenDeleteByText_thenRepositoryDeleteCalled() throws ToDoEmptyTextException {

		var text = "kek";
		toDoService.deleteByText(text);

		verify(toDoRepository, times(1)).deleteByText(text);

	}

	@Test
	void whenToDoTextEmpty_thenThrowToDoEmptyTextException() {
		assertThrows(ToDoEmptyTextException.class, () -> toDoService.getByText(""));
		assertThrows(ToDoEmptyTextException.class, () -> toDoService.deleteByText(""));
	}

}
