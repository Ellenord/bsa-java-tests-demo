package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoRepository toDoRepository;

	@Test
	void whenNotEmptyGetByText_thenReturnValidResponse() throws Exception {

		when(toDoRepository.findByText(anyString())).thenReturn(
			Arrays.asList(
				new ToDoEntity(5l, "im in the my me")
			)
		);
		
		this.mockMvc
			.perform(
				get("/todos/text")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"text\":\"im in the my me\"}")
			)
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value("im in the my me"))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());

	}

	@Test
	void whenEmptyGetByText_thenReturnBadRequest() throws Exception {

		when(toDoRepository.findByText(anyString())).thenReturn(
				Arrays.asList(
						new ToDoEntity(5l, "")
				)
		);

		this.mockMvc
				.perform(
						get("/todos/text")
								.contentType(MediaType.APPLICATION_JSON)
								.content("{\"text\":\"\"}")
				)
				.andExpect(status().isBadRequest());

	}

}
